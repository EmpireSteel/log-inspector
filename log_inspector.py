# coding=utf-8
#--------------------------------------------------------------------------------------------
# Version  2
#Author: Gavrilov Andrew
#Email: andrey.gavrilov@x5.ru
#--------------------------------------------------------------------------------------------
import time
import argparse
from datetime import datetime


def start_time(minutes):
    now = time.time()
    start = now - 60 * minutes
    return start


def error_finder(start, errors, log_files):
    find = 0
    for log in log_files:
        with open(log, 'r') as f:
            for line in f.readlines():
                if find == 0:
                    try:
                        date = time.mktime(datetime.strptime(line[:19], '%Y-%m-%d %H:%M:%S').timetuple())
                        if date >= start:
                            find = 1
                        else:
                            continue
                    except:
                        continue
                if find == 1:
                    for i in errors:
                        if i in line:
                            print 0
                            return
    print 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=u'Поиск ошибокок в лог файлах')
    parser.add_argument('-e', '--errors', help=u'Список ошибок. Задать в ковычках через ;')
    parser.add_argument('-l', '--logs', help=u'Путь к файлам логов(может быть несколько). Задать в ковычках через ;')
    parser.add_argument('-t', '--time', help=u'Расматриваемый промежуток времени. Задать в минутах.')
    args = parser.parse_args()
    errors = [item for item in args.errors.split('; ')]
    logs = [item for item in args.logs.split('; ')]
    minutes = int(args.time)
    start = start_time(minutes)
    error_finder(start, errors, logs)
